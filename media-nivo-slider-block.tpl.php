<?php
/**
 * @file
 * Template file for displaying a collection of media items in a Nivo Slider
 *
 * The template-specific available variables are:
 *
 * - $slider_id: The id of the nivo slider.
 * - $images: The collection of images to be in the slider.
 * - $image: Individual image item to be included in the slider.
 *    - $image['link']: Location for the image to link to.
 *    - $image['image]: Image tag with caption in the title attribute if it was provided.
 *
 */
?>

<div class="slider-wrapper theme-default">
  <div id="<?php echo $slider_id; ?>-media-nivo-slider">

    <?php 
      foreach($images as $image){
        $title = (isset($image['caption']) ? 'title="' . $image['caption'] . '"' : '');
        if (isset($image['link'])) {
          echo '<a href="' . $image['link'] . '">' . $image['image'] . '</a>'; 
        }
        else {
          echo $image['image']; 
         }
      }
    ?>
  </div>
</div>
