OVERVIEW
------------

The Media Nivo Slider module provides Media Gallery integration for the Nivo Slider jQuery plugin (http://nivo.dev7studios.com/).
This module provides the ability to take any media gallery and expose it's images as a Nivo Slider slideshow which is contained in a Drupal block.
These Nivo Slider blocks allow you to easily add a slideshow to various pages and in various regions of your site. 

The Nivo Slider blocks are individually customizable so each of your slideshows can have different transition effects, speeds, etc. 
In addition, since the Nivo Slider blocks are sourced from a media gallery, the ordering of images in the slideshow can easily be changed by simply re-ordering the media in the gallery.
This also allows for strictly curated slideshows and the ability to easily have the same image be a part of multiple slideshows if desired.

INSTALLATION
------------

1. Download and unpack the Media Nivo Slider module into your modules directory.

2. Download and unpack the module dependencies.
  a. Media Gallery Module (7.x-1.0-beta6) [http://drupal.org/project/media_gallery] - Download the media gallery module and its dependencies and unpack it into you modules directory.
  b. Libraries Module (7.x-1.0) [http://drupal.org/project/libraries] - Download the libraries module and unpack it into your modules directory.
  c. Nivo Slider jQuery plugin (2.6) [http://nivo.dev7studios.com/] - Download the Nivo Slider jQuery plugin and unpack it to your libraries folder, typically 'sites/all/libraries'. If you have Drush installed on your site you can use the following command to have Drush download and unpack the Nivo Slider plugin for you: 'media-nivo-slider-plugin'

3. Enable the modules.
   a. Visit your site's Administration > Modules page.
   b. Enable Media Nivo Slider. This will automatically enable all required modules.

GETTING STARTED
---------------

To get started with the Media Nivo Slider module you'll need to create a media gallery that will be the source of your slider images:

1. Go to Add content, and create a gallery.
2. When creating the gallery expand the 'Media Nivo Slider' fieldset and check the box to enable a nivo slider for this gallery.
3. The gallery will be empty by default, so use the "Add media" link to quickly upload a few images from your computer. 
4. The Nivo Slider will be follow the image ordering of the gallery so you can use drag-and-drop to rearrange the images to your liking.
5. Go to the Blocks administrative page.
6. Find and activate the Media Nivo Slider block for your gallery by adding it to a page region.
7. View the slider by navigating to a page that contains your block!

NIVO SLIDER SETTINGS
---------------

The majority of the nivo slider settings are configurable on a per slider basis. Slider settings are configured on the gallery edit form under the 'Media Nivo Slider' fieldset.
The following is a full list and description of the various settings that are configurable:

- Slide Transition Effect: The transition effect as the slider changes from one image to the next.
- Slide Transition Speed: How fast the slider transitions from one image to the next.
- Slide Pause Length: How long the slider should stay on one image before transitioning to the next.
- Slices: The number of segments the slider will break an image into for performing the image transitions.
- Caption Opacity: The opacity of the image captions.
- Pause Slideshow on Hover: Pause the slider from transitioning between images when hovering over the slider with the mouse.

ADDING IMAGE CAPTIONS/LINKS
---------------

Captions and images can be added to the slides via two fields that have been added to the image entities. 'Media Nivo Slider Caption' and 'Media Nivo Slider Link'.
Both of these fields can be found by either editing an image entity directly or using the 'Edit Media' tab on the gallery page.

USING IMAGE STYLES
---------------

The Media Nivo Slider module leverages the image styles functionality to allow you to easily resize, crop, saturate, etc the images in the slider. 
You can configure which image style, if any, a slider should use via the 'Image Style' field under the 'Media Nivo Slider' fieldset on the gallery edit form.

KNOWN ISSUES
---------------
1. You may encounter one of the following errors when installing the module via drush:
  - Module media_nivo_slider cannot be enabled because it depends on media  (7.x-1.0-beta5) but 7.x-1.0-beta5 is available
  - Module media_nivo_slider cannot be enabled because it depends on multiform  (1.0-beta2) but 7.x-1.0-beta2 is available
  This is due to the version dependencies set in the media_gallery.info file. See http://drupal.org/node/1180672 for more information.
  The current work around is to use the Drupal UI to enable the module.
