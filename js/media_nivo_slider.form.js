/**
 * @file
 *  Updates the Media Nivo Slider fieldset summary based on the status of the nivo slider block on the current media_gallery node.
 */

(function ($) {

Drupal.behaviors.mediaNivoSliderForm = {
  attach: function (context) {
    $('fieldset.media-nivo-slider-form', context).drupalSetSummary(function (context) {
      if ($('#edit-media-nivo-slider-block-und', context).attr('checked')) {
        return Drupal.t('Enabled');
      }
      else {
        return Drupal.t('Not enabled');
      }
    });
  }
};

})(jQuery);